﻿
Microsoft Visual Studio Solution File, Format Version 12.00
# Visual Studio Version 16
VisualStudioVersion = 16.0.29201.188
MinimumVisualStudioVersion = 10.0.40219.1
Project("{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}") = "Abarrotes", "Abarrotes\Abarrotes.csproj", "{7C42DD67-8A32-4C6A-B0E8-BD03A700D15C}"
EndProject
Project("{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}") = "AccesoaDatos.Abarrotes", "AccesoaDatos.Abarrotes\AccesoaDatos.Abarrotes.csproj", "{2B84124C-CA60-40CE-9168-AE28C92372AB}"
EndProject
Project("{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}") = "Entidades.Abarrotes", "Entidades.Abarrotes\Entidades.Abarrotes.csproj", "{726A11F2-E141-455E-9660-1FB5B8063C8C}"
EndProject
Project("{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}") = "Logicanegocios.Abarrotes", "Logicanegocios.Abarrotes\Logicanegocios.Abarrotes.csproj", "{403FD6E0-FBD7-4A9D-B9EA-F15BD92F0CC7}"
EndProject
Global
	GlobalSection(SolutionConfigurationPlatforms) = preSolution
		Debug|Any CPU = Debug|Any CPU
		Release|Any CPU = Release|Any CPU
	EndGlobalSection
	GlobalSection(ProjectConfigurationPlatforms) = postSolution
		{7C42DD67-8A32-4C6A-B0E8-BD03A700D15C}.Debug|Any CPU.ActiveCfg = Debug|Any CPU
		{7C42DD67-8A32-4C6A-B0E8-BD03A700D15C}.Debug|Any CPU.Build.0 = Debug|Any CPU
		{7C42DD67-8A32-4C6A-B0E8-BD03A700D15C}.Release|Any CPU.ActiveCfg = Release|Any CPU
		{7C42DD67-8A32-4C6A-B0E8-BD03A700D15C}.Release|Any CPU.Build.0 = Release|Any CPU
		{2B84124C-CA60-40CE-9168-AE28C92372AB}.Debug|Any CPU.ActiveCfg = Debug|Any CPU
		{2B84124C-CA60-40CE-9168-AE28C92372AB}.Debug|Any CPU.Build.0 = Debug|Any CPU
		{2B84124C-CA60-40CE-9168-AE28C92372AB}.Release|Any CPU.ActiveCfg = Release|Any CPU
		{2B84124C-CA60-40CE-9168-AE28C92372AB}.Release|Any CPU.Build.0 = Release|Any CPU
		{726A11F2-E141-455E-9660-1FB5B8063C8C}.Debug|Any CPU.ActiveCfg = Debug|Any CPU
		{726A11F2-E141-455E-9660-1FB5B8063C8C}.Debug|Any CPU.Build.0 = Debug|Any CPU
		{726A11F2-E141-455E-9660-1FB5B8063C8C}.Release|Any CPU.ActiveCfg = Release|Any CPU
		{726A11F2-E141-455E-9660-1FB5B8063C8C}.Release|Any CPU.Build.0 = Release|Any CPU
		{403FD6E0-FBD7-4A9D-B9EA-F15BD92F0CC7}.Debug|Any CPU.ActiveCfg = Debug|Any CPU
		{403FD6E0-FBD7-4A9D-B9EA-F15BD92F0CC7}.Debug|Any CPU.Build.0 = Debug|Any CPU
		{403FD6E0-FBD7-4A9D-B9EA-F15BD92F0CC7}.Release|Any CPU.ActiveCfg = Release|Any CPU
		{403FD6E0-FBD7-4A9D-B9EA-F15BD92F0CC7}.Release|Any CPU.Build.0 = Release|Any CPU
	EndGlobalSection
	GlobalSection(SolutionProperties) = preSolution
		HideSolutionNode = FALSE
	EndGlobalSection
	GlobalSection(ExtensibilityGlobals) = postSolution
		SolutionGuid = {A6B4ADE5-FDE5-4334-9A0D-7DE1565F43DA}
	EndGlobalSection
EndGlobal
